# Hackerrank exercises

Bookmark repo for all [Hackerrank](https://www.hackerrank.com/) exercises attempted.
Mostly solved with PHP.

## Exercises:
- [Beautiful Binary String](beautiful-binary-string.php)
- [Counting Valleys](counting-valleys.php)
- [Drawing Book](drawing-book.php)
- [Electronics Shop](electronics-shop.php)
- [Find The Median](find-the-median.php)
- [Game of Thrones](game-of-thrones.php)
- [Hackerrank in a string](hackerrank-in-a-string.php)
- [Halloweeen Party](halloween-party.php)
- [Ice-cream Parlor](icecream-parlor.py) (`Python`)
- [Jumping on the clouds](jumping-on-the-clouds.php)
- [Lonely Integer](lonely-integer.php)
- [Making anagrams](making-anagrams.php)
- [Palindrome Index](palindrome-index.php)
- [Repeated String](repeated-string.php)
- [Running time](runningtime.php)
- [Sherlock and the valid string](sherlock-and-valid-string.php)
- [Sock Merchant](sock-merchant.php)~~~~
- [String Construction](string-construction.php)
- [Utopian tree](utopian-tree.php)

## Algorithms Sorting
- [Insertion Sort 1](insertionsort1.php) | ([Python version](insertionsort1.py))
- [Insertion Sort 2](insertionsort2.php) | ([Python version](insertionsort2.py))